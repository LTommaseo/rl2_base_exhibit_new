/**
 * Created by rturner on 15/01/2014.
 */
package exhibit.model {
import org.osflash.signals.Signal;

public class ModelUpdatedSignal extends Signal{
    public function ModelUpdatedSignal() {
        super();
    }
}
}
