/**
 * Created by rturner on 14/01/2014.
 */
package exhibit.model {
public class AndroMediaPlayerModel{

    private var _isLicensed:Boolean = false;
    private var _customer_id:String;
    private var _shortPlayer_id:String;
    private var _player_id:String;

    [Inject]
    public var signal:ModelUpdatedSignal;

    public function AndroMediaPlayerModel() {
    }

    public function get isLicensed():Boolean {
        return _isLicensed;
    }

    public function set isLicensed(value:Boolean):void {
        _isLicensed = value;
    }

    public function get customer_id():String {
        return _customer_id;
    }

    public function set customer_id(value:String):void {
        _customer_id = value;
    }

    public function get shortPlayer_id():String {
        return _shortPlayer_id;
    }

    public function set shortPlayer_id(value:String):void {
        _shortPlayer_id = value;
    }

    public function get player_id():String {
        return _player_id;
    }

    public function set player_id(value:String):void {
        _player_id = value;
    }
}
}
