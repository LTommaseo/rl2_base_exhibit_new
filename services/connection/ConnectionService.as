/**
 * Created by rturner on 13/01/2014.
 */
package exhibit.services.connection {
import exhibit.services.connection.signals.ConnectionDataSignal;
import exhibit.services.connection.signals.ConnectionSignal;

import com.greensock.TweenMax;
import com.pnwrain.flashsocket.FlashSocket;
import com.pnwrain.flashsocket.events.FlashSocketEvent;

import flash.events.Event;

import flash.events.IOErrorEvent;

import org.osflash.signals.Signal;


public class ConnectionService implements IConnectionService {

    [Inject]
    public  var connectionVO:ConnectionVO;

    [Inject]
    public var connectionDataVO:ConnectionDataVO;

    [Inject]
    public var connectionSignal:ConnectionSignal;

    [Inject]
    public var connectionDataSignal:ConnectionDataSignal;

    private var websocket:FlashSocket;

    private var _socket_connected:Signal;
    private var _socket_disconnected:Signal;



    public function ConnectionService() {


        _socket_connected = new Signal();
        _socket_disconnected = new Signal();

    }

    public function connectToSocket():void {


        this.removeSocketEventsDestroySocket();
        websocket = new FlashSocket('ws://clientsmanager-exhibit.rhcloud.com:8000');
//        websocket = new FlashSocket('wss://192.168.1.119:8081');

        trace("service connecting to socket:");
        addSocketEvents();

    }

    public function disconnect():void {
        websocket.dispatchEvent(new FlashSocketEvent(FlashSocketEvent.CLOSE));
    }

    public function get socket_connected():Signal {
        return _socket_connected;
    }

    public function get socket_disconnected():Signal {
        return _socket_disconnected;
    }


    //  SOCKET MANAGEMENT FUNCTIONS

    private function retryConnection():void {
        TweenMax.delayedCall(1000, function():void{
            connectToSocket();
        })
    }

    private function onIOError(event:IOErrorEvent = null):void {

        trace("can't connect via web, connecting via local network");

        retryConnection()

    }



    private function handleWebSocketMessage(event:FlashSocketEvent):void {
//        trace("socket message");

//        var data = JSON.parse(event.data);
        trace("message received "+event.data);

        connectionDataVO.action = "receive";
        trace("connectionService DATA: "+event.data);
        connectionDataVO.data = event.data;

        connectionDataSignal.dispatch();




//        _starling.root.stage.dispatchEvent(new SocketEvent(SocketEvent.SOCKET_EVENT, data));



    }

    private function handleWebSocketOpen(event:FlashSocketEvent):void {

        trace("socket connected");

        _socket_connected.dispatch();
        connectionVO.isConnected = true;
        connectionSignal.dispatch(connectionVO);


    }

    private function handleWebSocketClosed(event:FlashSocketEvent):void {
        trace("Disconnected");
        _socket_disconnected.dispatch();
    }

    private function handleConnectionFail(event:FlashSocketEvent):void {
        trace("Connection Failure: " + event.data);
        _socket_disconnected.dispatch();
    }

    private function onSecurityError(event:FlashSocketEvent):void {
    trace("security error ");
    }


    // SOCKET EVENTS

    private function addSocketEvents():void {
        websocket.addEventListener(FlashSocketEvent.CLOSE, handleWebSocketClosed);
        websocket.addEventListener(FlashSocketEvent.CONNECT, handleWebSocketOpen);
        websocket.addEventListener(FlashSocketEvent.CONNECT_ERROR, handleConnectionFail);
        websocket.addEventListener(FlashSocketEvent.MESSAGE, handleWebSocketMessage);
        websocket.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
        websocket.addEventListener(FlashSocketEvent.SECURITY_ERROR, onSecurityError);
    }



    private function removeSocketEventsDestroySocket():void {

        if(!websocket) return;

        websocket.removeEventListener(FlashSocketEvent.CLOSE, handleWebSocketClosed);
        websocket.removeEventListener(FlashSocketEvent.CONNECT, handleWebSocketOpen);
        websocket.removeEventListener(FlashSocketEvent.CONNECT_ERROR, handleConnectionFail);
        websocket.removeEventListener(FlashSocketEvent.MESSAGE, handleWebSocketMessage);
        websocket.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
        websocket.removeEventListener(FlashSocketEvent.SECURITY_ERROR, onSecurityError);
        websocket = null;
    }


    public function send(data:Object, event:String):void {
        websocket.emit(event, data);
    }
}
}
