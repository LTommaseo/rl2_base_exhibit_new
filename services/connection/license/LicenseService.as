/**
 * Created by rturner on 14/01/2014.
 */
package exhibit.services.connection.license {
import exhibit.model.AndroMediaPlayerModel;
import exhibit.services.connection.ConnectionDataVO;
import exhibit.services.connection.ConnectionVO;
import exhibit.services.connection.signals.ConnectionDataSignal;
import exhibit.services.connection.signals.ConnectionSignal;

import flash.net.SharedObject;

import mx.utils.UIDUtil;

public class LicenseService implements ILicenseService {
    private var preferences:SharedObject;
    private var _playerID:String;
    private var shortPlayerID:String;

    [Inject]
    public var signal:ConnectionDataSignal;

    [Inject]
    public var vo:ConnectionDataVO;

    [Inject]
    public var model:AndroMediaPlayerModel;


    public function LicenseService() {

    }

    public function checkForID():void {

        preferences = SharedObject.getLocal("AM_prefs");



        if(preferences.data.playerID){
            _playerID = preferences.data.playerID;
        } else {
            _playerID = UIDUtil.createUID();
            preferences.data.playerID = _playerID;
        }

        shortPlayerID = _playerID.substr(0, 8);

        model.player_id = _playerID;
        model.shortPlayer_id = shortPlayerID;
        model.signal.dispatch();

        trace("short ID "+shortPlayerID);

        vo.action = "send";

        vo.event = "clientRegistration";

        vo.data = {shortPlayerID: shortPlayerID, playerID: _playerID};

        signal.dispatch();


    }
}
}
