/**
 * Created by rturner on 13/01/2014.
 */
package exhibit.services.connection {
import org.osflash.signals.Signal;

public interface IConnectionService {


     function connectToSocket():void

    function get socket_connected():Signal;
    function get socket_disconnected():Signal;





    }
}
