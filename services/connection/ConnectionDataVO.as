/**
 * Created by rturner on 13/01/2014.
 */
package exhibit.services.connection {
public class ConnectionDataVO {


    private var _action:String;
    private var _event:String;
    private var _data:Object;

    public function ConnectionDataVO() {
    }





    public function get action():String {
        return _action;
    }

    public function set action(value:String):void {
        _action = value;
    }



    public function get event():String {
        return _event;
    }

    public function set event(value:String):void {
        _event = value;
    }

    public function get data():Object {
        return _data;
    }

    public function set data(value:Object):void {
        _data = value;
    }
}
}
