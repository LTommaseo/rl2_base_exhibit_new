/**
 * Created by rturner on 13/01/2014.
 */
package exhibit.services.connection {
public class ConnectionVO {

    private var _isConnected:Boolean;
    private var _message:*;
    private var _action:String;
    private var _isLicensed:Boolean = false;
    private var _event:String;
    private var _data:Object;

    public function ConnectionVO() {
    }

    public function get isConnected():Boolean {
        return _isConnected;
    }

    public function set isConnected(value:Boolean):void {
        _isConnected = value;
    }

    public function get message():* {
        return _message;
    }

    public function set message(value:*):void {
        _message = value;
    }

    public function get action():String {
        return _action;
    }

    public function set action(value:String):void {
        _action = value;
    }

    public function get isLicensed():Boolean {
        return _isLicensed;
    }

    public function set isLicensed(value:Boolean):void {
        _isLicensed = value;
    }

    public function get event():String {
        return _event;
    }

    public function set event(value:String):void {
        _event = value;
    }

    public function get data():Object {
        return _data;
    }

    public function set data(value:Object):void {
        _data = value;
    }
}
}
