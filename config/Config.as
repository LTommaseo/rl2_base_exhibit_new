package exhibit.config {
import exhibit.commands.AppReadyCommand;
import exhibit.commands.SetupCommand;
import exhibit.model.ModelUpdatedSignal;
import exhibit.signals.PerformSetup;
import exhibit.views.IMainApplication;
import exhibit.views.MainApplicationMediator;
import exhibit.views.Settings;
import exhibit.views.SettingsMediator;
import exhibit.views.buttons.ConnectionBtn;
import exhibit.views.buttons.ConnectionBtnMediator;

import baseapp.Actor;
import baseapp.controller.console.ConsoleAddLogCommand;
import baseapp.controller.console.ConsoleDebuggingFeaturesCommand;
import baseapp.controller.console.ConsoleDisplayErrorCommand;
import baseapp.controller.console.events.ConsoleErrorEvent;
import baseapp.controller.console.events.ConsoleLogEvent;
import baseapp.controller.mainLoaders.ContentCreateCommand;
import baseapp.controller.mainLoaders.LoadFontsCommand;
import baseapp.controller.screen.DisplayConfigCommand;
import baseapp.controller.screen.events.DisplayConfigEvent;
import baseapp.controller.timeout.TimeoutCommand;
import baseapp.controller.timeout.events.TimeoutManagementEvent;
import baseapp.model.ContentModel;
import baseapp.model.SettingsModel;
import baseapp.service.ContentLoaderService;
import baseapp.service.SettingsLoaderService;
import baseapp.service.events.ContentLoadCompleteEvent;
import baseapp.service.events.FontLoadedEvent;
import baseapp.service.events.SettingsLoadCompleteEvent;

import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
import robotlegs.bender.extensions.signalCommandMap.api.ISignalCommandMap;
import robotlegs.bender.framework.api.IContext;
import robotlegs.bender.framework.api.IInjector;
import robotlegs.bender.framework.api.LogLevel;

public class Config {
    [Inject]
    public var context:IContext;

    [Inject]
    public var mediatorMap:IMediatorMap;

    [Inject]
    public var commandMap:ISignalCommandMap;

    [Inject]
    public var eventMap:IEventCommandMap;


    //modified by Ondina - 01.10.2013
    /*[Inject]
     public var injector:Injector;*/
    [Inject]
    public var injector:IInjector;

    //---------------------------------------------------------------
    // Configuration
    //---------------------------------------------------------------

    [PostConstruct]
    public function init():void {
        //Provide fallback if something goes wrong...
        //modified by Ondina - 01.10.2013
        //injector.fallbackProvider = new DefaultFallbackProvider();

        context.logLevel = LogLevel.DEBUG;


        // MODELS
        injector.map(SettingsModel).asSingleton();
        injector.map(ContentModel).asSingleton();

        // SERVICES
        injector.map(SettingsLoaderService).asSingleton();
        injector.map(ContentLoaderService).asSingleton();

        //CONTENT
//            commandMap.mapEvent(SettingsLoadCompleteEvent.SETTINGS_LOADER_COMPLETE, ContentCreateCommand, SettingsLoadCompleteEvent);
        eventMap.map(SettingsLoadCompleteEvent.SETTINGS_LOADER_COMPLETE).toCommand(ContentCreateCommand);
        eventMap.map(ContentLoadCompleteEvent.CONTENT_LOAD_COMPLETE).toCommand(AppReadyCommand);


        // DISPLAY MANAGEMENT
        eventMap.map(DisplayConfigEvent.DISPLAY_CONFIG).toCommand(DisplayConfigCommand);

        //DEBUGGING FEATURES

        eventMap.map(SettingsLoadCompleteEvent.SETTINGS_LOADER_COMPLETE).toCommand(ConsoleDebuggingFeaturesCommand);
        eventMap.map(ConsoleLogEvent.LOG).toCommand(ConsoleAddLogCommand);
        eventMap.map(ConsoleErrorEvent.ERROR).toCommand(ConsoleDisplayErrorCommand);

        // FONTS

        eventMap.map(FontLoadedEvent.COMPLETE).toCommand(LoadFontsCommand);

        //TIMEOUT

        eventMap.map(TimeoutManagementEvent.TIMEOUT_CREATE).toCommand(TimeoutCommand);
        eventMap.map(TimeoutManagementEvent.TIMEOUT_START).toCommand(TimeoutCommand);
        eventMap.map(TimeoutManagementEvent.TIMEOUT_STOP).toCommand(TimeoutCommand);

        // and finally the event that makes it all start

//            commandMap.mapEvent(AppReadyEvent.READY, MainCommand, AppReadyEvent);
//            eventMap.map(AppReadyEvent.READY).toCommand(AppReadyCommand);


        // adding injection to port RL1 files

        injector.map(Actor);


        //SIGNALS
        injector.map(ModelUpdatedSignal).asSingleton();


//            injector.map(AndroMediaPlayerModel).asSingleton();

//            injector.map(ConnectionService).asSingleton();
//            injector.map(ConnectionVO).asSingleton();
//            injector.map(ConnectionDataVO).asSingleton();
//            injector.map(MetalWorksMobileTheme).asSingleton();


//            injector.map(LicenseService).asSingleton();
//            injector.map(ConnectionSignal)


//            commandMap.map(ConnectionSignal).toCommand(ConnectionCommand);
//            commandMap.map(ConnectionDataSignal).toCommand(ConnectionDataCommand);
//            commandMap.map(ConnectionSignal).toCommand(LicenseCommand);
//            commandMap.map(ConnectionDataSignal).toCommand(LicenseResponseCommand);


        // LEGACY COMMANDS PORTED FROM RL1 TO LOAD INITIAL FILES

//            eventMap.map(SettingsCreateEvent.SETTINGS_LOAD_COMPLETE)


        //modified by Ondina - 01.10.2013
//			commandMap.map(PerformSetup, true).toCommand(SetupCommand);
        commandMap.map(PerformSetup).toCommand(SetupCommand);

        //			  Map mediators

//			mediatorMap.map(IButton).toMediator(ButtonMediator);
        mediatorMap.map(ConnectionBtn).toMediator(ConnectionBtnMediator);
        mediatorMap.map(IMainApplication).toMediator(MainApplicationMediator);
        mediatorMap.map(Settings).toMediator(SettingsMediator);
    }
}
}
