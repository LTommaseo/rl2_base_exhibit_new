/**
 * Created by rturner on 13/01/2014.
 */
package exhibit {
import exhibit.config.Config;
import exhibit.views.MainApplication;

import away3d.core.managers.Stage3DManager;
import away3d.core.managers.Stage3DProxy;
import away3d.events.Stage3DEvent;

import flash.display.Sprite;
import flash.display.StageAlign;
import flash.display.StageScaleMode;
import flash.events.Event;
import flash.events.TimerEvent;
import flash.geom.Rectangle;
import flash.utils.Timer;

import robotlegs.bender.bundles.SARSBundle;
import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.extensions.sarsIntegration.api.StarlingCollection;

import robotlegs.bender.framework.api.IContext;
import robotlegs.bender.framework.impl.Context;

import starling.core.Starling;
import starling.events.ResizeEvent;


public class App extends Sprite {

    private var timer:Timer;

    //---------------------------------------------------------------
    // Starling variables
    //---------------------------------------------------------------

    private var _starlingUI:Starling;
	
	/*testing git*/
//    private var _starlingBackground:Starling; 

    //---------------------------------------------------------------
    // Common variables
    //---------------------------------------------------------------

    private var _stage3DManager:Stage3DManager;
    private var _stage3DProxy:Stage3DProxy;
    private var _context:IContext;

    public function App() {

        trace("exhibit BOOTSTRAP");
        super();
        addEventListener(flash.events.Event.ADDED_TO_STAGE, onAddedToStage);
    }

    private function onAddedToStage(event:Event):void {
        removeEventListener(flash.events.Event.ADDED_TO_STAGE, onAddedToStage);
        stage.scaleMode = StageScaleMode.NO_SCALE;
        stage.align = StageAlign.TOP_LEFT;

        timer = new Timer(200, 0);
        timer.addEventListener(TimerEvent.TIMER, onTimer);
        timer.start();


    }

    private function onTimer(event:TimerEvent):void {
        timer.stop();

        _stage3DManager = Stage3DManager.getInstance(stage);

        _stage3DProxy = _stage3DManager.getFreeStage3DProxy();
        _stage3DProxy.addEventListener(Stage3DEvent.CONTEXT3D_CREATED, onCreate);
    }

    private function onCreate(event:Stage3DEvent):void {

        _stage3DProxy.removeEventListener(Stage3DEvent.CONTEXT3D_CREATED, onCreate);
        initStarling();

        _context = new Context();

        var starlingCollection:StarlingCollection = new StarlingCollection();
        starlingCollection.addItem(_starlingUI, "ui");

        _context.install(SARSBundle).configure(starlingCollection, starlingCollection, Config, new ContextView(this));

        stage.addEventListener(flash.events.Event.ENTER_FRAME, onEnterFrame, false, 0, true);

    }

    private function initStarling():void {
        Starling.handleLostContext = true;
        _starlingUI = new Starling(MainApplication, stage, _stage3DProxy.viewPort, _stage3DProxy.stage3D);
        _starlingUI.stage.addEventListener(starling.events.Event.RESIZE, onResize)
        _starlingUI.stage.color = 0xffffff;
//        _starlingUI.stage3D.visible = false;
        _starlingUI.start();
    }

    /** This event handler is called when the device rotates. */
    private function onResize(event:ResizeEvent):void
    {
        trace("stage resize");
        updateDimensions(event.width, event.height);
//        updatePositions(event.width, event.height);
    }

    /** Updates the size of stage and viewPort depending on the
     *  current screen size in PIXELS. */
    private function updateDimensions(width:int, height:int):void
    {
        trace("resize to w:"+width+" h:"+height);
        var scale:Number = Starling.current.contentScaleFactor;
        var viewPort:Rectangle = new Rectangle(0, 0, width, height);

        Starling.current.viewPort = viewPort;
        _stage3DProxy.width = viewPort.width;
        _stage3DProxy.height = viewPort.height;
        _starlingUI.stage.stageWidth  = viewPort.width  / scale;
        _starlingUI.stage.stageHeight = viewPort.height / scale;
    }

    private function onEnterFrame( event:flash.events.Event ):void
    {
        _stage3DProxy.clear();


        _starlingUI.nextFrame();

        _stage3DProxy.present();
    }
}
}
