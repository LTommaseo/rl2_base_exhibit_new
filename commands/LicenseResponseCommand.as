/**
 * Created by rturner on 14/01/2014.
 */
package exhibit.commands {
import exhibit.model.AndroMediaPlayerModel;
import exhibit.services.connection.ConnectionDataVO;
import exhibit.services.connection.ConnectionVO;
import exhibit.services.connection.license.LicenseService;
import exhibit.services.connection.signals.ConnectionDataSignal;

import robotlegs.bender.bundles.mvcs.Command;

public class LicenseResponseCommand extends Command{

    [Inject]
    public var signal:ConnectionDataSignal;


    [Inject]
    public var connectionVO:ConnectionVO;

    [Inject]
    public var vo:ConnectionDataVO;

    [Inject]
    public var licenseService:LicenseService;

    [Inject]
    public var model:AndroMediaPlayerModel;

    public function LicenseResponseCommand() {





    }

    override public function execute():void {

//        var data:Object = JSON.parse(vo.data);

        if(vo.action == "receive"){
            trace("License response, licensed or not? = "+vo.action);

           deepTrace(vo.data);

            trace("data: "+vo.data["licensed"]);

            if(vo.data["licensed"] == true){
               model.isLicensed = true;
               model.customer_id = vo.data["customer_id"];
                model.signal.dispatch();
            }
        }







    }

    public function deepTrace( obj : *, level : int = 0 ) : void{
        var tabs : String = "";
        for ( var i : int = 0 ; i < level ; i++, tabs += "\t" );

        for ( var prop : String in obj ){
            trace( tabs + "[" + prop + "] -> " + obj[ prop ] );
            deepTrace( obj[ prop ], level + 1 );
        }
    }
}
}
