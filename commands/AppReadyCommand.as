package exhibit.commands {
import baseapp.controller.console.events.ConsoleLogEvent;
import baseapp.service.events.ContentLoadCompleteEvent;

import flash.events.IEventDispatcher;

import robotlegs.bender.bundles.mvcs.Command;

public class AppReadyCommand extends Command {
    [Inject]
    public var event:ContentLoadCompleteEvent;

    [Inject]
    public var iEventDispatcher:IEventDispatcher;

    override public function execute():void {

        trace("AppReadyCommand instantiate main application");

        iEventDispatcher.dispatchEvent(new ConsoleLogEvent( ConsoleLogEvent.LOG, "Application ready starting now!!") );


    }
}
}