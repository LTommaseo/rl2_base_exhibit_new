package exhibit.commands {

import baseapp.Actor;
import baseapp.service.SettingsLoaderService;

import feathers.themes.MetalWorksMobileTheme;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;

import starling.display.DisplayObjectContainer;

public class SetupCommand extends Command {

//    SERVICES

    [Inject]
    public var eventMap:IEventCommandMap;

    [Inject]
    public var settingsLoaderService:SettingsLoaderService;

    [Inject(name="ui")]
    public var starlingUI:DisplayObjectContainer;

    [Inject]
    public var actor:Actor;


    override public function execute():void {

        trace("exhibit setup command");


        new MetalWorksMobileTheme(null, false);


        settingsLoaderService.load();


    }


}
}