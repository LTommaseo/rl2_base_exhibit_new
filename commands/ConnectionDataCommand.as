/**
 * Created by rturner on 14/01/2014.
 */
package exhibit.commands {
import exhibit.services.connection.ConnectionDataVO;
import exhibit.services.connection.ConnectionService;

import robotlegs.bender.bundles.mvcs.Command;

public class ConnectionDataCommand extends Command{

    [Inject]
    public var vo:ConnectionDataVO;

    [Inject]
    public var service:ConnectionService;

    public function ConnectionDataCommand() {
    }

    override public function execute():void {
        trace ("ConnectionDataCommand:"+vo.action);

        if (vo.action == "send"){
            service.send(vo.data, vo.event);
        }
    }
}
}
