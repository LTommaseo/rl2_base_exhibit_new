/**
 * Created by rturner on 14/01/2014.
 */
package exhibit.commands {
import exhibit.model.AndroMediaPlayerModel;
import exhibit.services.connection.ConnectionService;
import exhibit.services.connection.ConnectionVO;
import exhibit.services.connection.license.LicenseService;
import exhibit.services.connection.signals.ConnectionSignal;

import robotlegs.bender.bundles.mvcs.Command;

public class LicenseCommand extends Command{

    [Inject]
    public var signal:ConnectionSignal;

    [Inject]
    public var connectionService:ConnectionService;

    [Inject]
    public var connectionVO:ConnectionVO;

    [Inject]
    public var licenseService:LicenseService;

    [Inject]
    public var model:AndroMediaPlayerModel;

    public function LicenseCommand() {





    }

    override public function execute():void {
        trace("License command socket connected = "+connectionVO.isConnected);


        if(!model.isLicensed){
            if(connectionVO.isConnected && !connectionVO.isLicensed) {
                connectionVO.action = "";
                licenseService.checkForID();
            }
        }



    }
}
}
