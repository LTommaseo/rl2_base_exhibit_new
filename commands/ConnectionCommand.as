/**
 * Created by rturner on 13/01/2014.
 */
package exhibit.commands {
import exhibit.services.connection.ConnectionService;
import exhibit.services.connection.ConnectionVO;
import exhibit.services.connection.IConnectionService;
import exhibit.services.connection.signals.ConnectionSignal;

import robotlegs.bender.bundles.mvcs.Command;

public class ConnectionCommand extends Command {

    [Inject]
    public var signal:ConnectionSignal;

    [Inject]
    public var connectionService:ConnectionService;

    [Inject]
    public var connectionVO:ConnectionVO;

    public function ConnectionCommand() {


    }

    override public function execute():void {
//        trace(signal.vo.action);

        trace("signal action:"+connectionVO.action);
        trace("signal isConnected:"+connectionVO.isConnected);
        trace("signal message:"+connectionVO.message);
        trace("signal event:"+connectionVO.event);


        if(!connectionVO.isConnected && connectionVO.action == "connect") {
            connectionVO.action == "";
            connectionService.connectToSocket();
        }

        if(connectionVO.isConnected && connectionVO.action == "disconnect") {
            connectionVO.action == "";
            connectionService.disconnect();
        }

        if(connectionVO.isConnected && connectionVO.action == "message") {
            connectionVO.action == "";
        }

    }

}

}
