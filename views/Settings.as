/**
 * Created by rturner on 15/01/2014.
 */
package exhibit.views {
import feathers.controls.Button;

import org.osflash.signals.Signal;

import starling.display.Quad;

import starling.display.Sprite;
import starling.events.Event;
import starling.text.TextField;
import starling.text.TextFieldAutoSize;
import starling.utils.HAlign;
import starling.utils.VAlign;

public class Settings extends Sprite {

    private var panel:Sprite;
    private var _connectionBtn:Button = new Button();

    private var _connectionBtnSignal:Signal = new Signal();

    private var _info:TextField;

    public function Settings() {


        addEventListener(Event.ADDED_TO_STAGE, onAdded);


    }

    private function onAdded(event:Event):void {
        addEventListener(Event.RESIZE, onResize);
        removeEventListener(Event.ADDED_TO_STAGE, onAdded);

        panel = new Sprite();

//        _connectionBtn = new Button();
        _connectionBtn.addEventListener(Event.TRIGGERED, onConnectionBtnTriggered);
        _connectionBtn.label = "connection";
        _connectionBtn.x = _connectionBtn.y = 3;
        panel.addChild(_connectionBtn);

        _info = new TextField( stage.stageWidth - 130, 50, "")
        _info.text = "info";
        _info.x = 125;
        _info.bold = false;
        _info.fontSize = 16;
        _info.autoSize = TextFieldAutoSize.VERTICAL;
        _info.hAlign = HAlign.CENTER;
        _info.vAlign = VAlign.CENTER;
        panel.addChildAt(_info, 0 );

        var background:Quad = new Quad(stage.stageWidth, 50, 0xffffff, true);
        background.alpha = .5
        panel.addChildAt(background, 0 );
        this.addChild(panel);

    }

    private function onResize(event:Event):void {
        _info.width = stage.stageWidth - 130;
    }

    private function onConnectionBtnTriggered(event:Event):void {
        _connectionBtnSignal.dispatch(new Object());
    }

    public function get connectionBtn():Button {
        return _connectionBtn;
    }

    public function get connectionBtnSignal():Signal {
        return _connectionBtnSignal;
    }

    public function get info():TextField {
        return _info;
    }

    public function set info(value:TextField):void {
        _info = value;
    }
}
}
