/**
 * Created by rturner on 15/01/2014.
 */
package exhibit.views {
import exhibit.model.AndroMediaPlayerModel;
import exhibit.services.connection.ConnectionVO;
import exhibit.services.connection.signals.ConnectionSignal;

import robotlegs.bender.bundles.mvcs.Mediator;

public class SettingsMediator extends Mediator {


    [Inject]
    public var view:Settings;

    [Inject]
    public var connectionSignal:ConnectionSignal;

    [Inject]
    public var connectionVO:ConnectionVO;

    [Inject]
    public var model:AndroMediaPlayerModel;



    public function SettingsMediator() {
    }

    override public function initialize():void {

        connectionSignal.add(onConnectionSignal);
        view.connectionBtnSignal.add(onConnectionButtonSignal);
        model.signal.add(onModelChange);
    }

    private function onModelChange():void {

        if(!model.isLicensed) {
            if(model.shortPlayer_id) view.info.text = "please use "+model.shortPlayer_id+" to add player to Andromedia manager";
        } else {
            if(model.customer_id) view.info.text = "Player "+ model.shortPlayer_id +" licensed to customer "+model.customer_id;
        }

    }

    private function onConnectionButtonSignal(o:Object):void {
        if(connectionVO.isConnected){
            connectionVO.action = "disconnect";
        } else {
            connectionVO.action = "connect";
        }
        connectionSignal.dispatch(connectionVO);
    }

    private function onConnectionSignal(o:Object):void {

        trace(connectionVO.isConnected);
        if(connectionVO.isConnected){
            view.connectionBtn.label = "Reconnect";
        } else {
            view.connectionBtn.label = "Connect";
        }


    }
}
}
