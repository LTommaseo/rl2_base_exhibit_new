package exhibit.views
{
import exhibit.services.connection.ConnectionVO;
import exhibit.services.connection.signals.ConnectionSignal;

import robotlegs.bender.bundles.mvcs.Mediator;
import exhibit.signals.PerformSetup;


public class MainApplicationMediator extends Mediator
{
    [Inject]
    public var view:IMainApplication;

    [Inject]
    public var performSetup:PerformSetup;

//    [Inject]
//    public var connectionSignal:ConnectionSignal;
//
//    [Inject]
//    public var connectionVO:ConnectionVO;

    override public function initialize():void
    {

        trace("mainApplicationMediator");
        super.initialize();

        performSetup.dispatch();

//        var vo:ConnectionVO = new ConnectionVO();
//        vo.action = "connect";
//        connectionVO.action = "connect";
//        connectionSignal.dispatch(connectionVO);
    }

}
}