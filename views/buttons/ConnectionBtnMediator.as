/**
 * Created by rturner on 13/01/2014.
 */
package exhibit.views.buttons {
import exhibit.services.connection.ConnectionVO;
import exhibit.services.connection.signals.ConnectionSignal;

import feathers.themes.MetalWorksMobileTheme;

import robotlegs.bender.bundles.mvcs.Mediator;

public class ConnectionBtnMediator extends Mediator{

    [Inject]
    public var view:ConnectionBtn;

    [Inject]
    public var connectionSignal:ConnectionSignal;

    [Inject]
    public var connectionVO:ConnectionVO;

    [Inject]
    public var scheme:MetalWorksMobileTheme;

    public function ConnectionBtnMediator() {
    }

    override public function initialize():void {

        trace(scheme);
        connectionSignal.add(onSignal);
//        view.defaultSkin = scheme;
    }

    private function onSignal(o:*):void {
        trace(connectionVO.isConnected);

        connectionVO.isConnected ? view.label = "disconnect" : view.label = "connect";

    }

}
}
